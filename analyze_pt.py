#!/usr/bin/python3

import sys
import os
import json
import page_table as pt
import lzma

raw_mem_filename = '/dev/mem'
pid2cr3_filename = '/sys/kernel/pid2cr3/pid2cr3'

PT_node_counter = [0, 0, 0, 0] # Count per level
alloced_page_counter = [0, 0, 0] # 1GB, 2MB, 4KB pages
per_level_ptrs = [ [], [], []] # 1GB, 2MB, 4KB page pointers

def pid2cr3(pid):
    with open(pid2cr3_filename, 'w') as fp:
      fp.write("%d" % pid)
    with open(pid2cr3_filename, 'r') as fp:
      cr3 = fp.read()

    return int(cr3, 16)


def get_cr3(pid):
    cr3_int = pid2cr3(pid)
    cr3 = pt.Cr3(cr3_int)
    return cr3

leaf_nodes = []

def seek_bin_fp(fp, pt_pa, addr_dict):
    bin_offset = addr_dict[str(pt_pa)]
    return fp.seek(bin_offset, os.SEEK_SET)

def traverse_pt_level(level, base_va, pt_pa, fp, addr_dict):
    global PT_node_counter, alloced_page_counter, leaf_nodes, per_level_ptrs
    if level == 0:
        return


    PTL = pt.PTL
#    PT_Raw = pt.RawPT(0, pt_pa)

    ret = seek_bin_fp(fp, pt_pa, addr_dict)

    pt_node = fp.read(4096)
    leaf_nodes.append(pt_node)
    PT_node_counter[4-level] += 1

    iterate_range = 512
    if level == 4:
        iterate_range = 256 # Don't want to iterate through upper half of VA (kernel space)

    for i in range(iterate_range):
        pte_bin = pt_node[i * 8:i * 8 + 8]
        pte_int = int.from_bytes(pte_bin, byteorder = sys.byteorder)
        va = base_va + i * (1 << (12 + 9*(level-1)))
        pte = PTL(pte_int, va, level)

        if pte.present:
            print(' ' * (4-level) + "[%3d]"%i +  str(pte))
            if pte.isLeaf():
                # Don't attempt to traverse if PT entry is a leaf entry (points to actual page)
                idx = 4-level - 1
                alloced_page_counter[idx] += 1
                per_level_ptrs[idx].append(pte.get_effective_ptr_bits())
            else:
                traverse_pt_level(level-1, va, pte.get_ptr(), fp, addr_dict)

def nearestPow2Order(val):
    order = 1
    pow2 = 1 << order

    while val > pow2:
        order+=1
        pow2 = 1 << order

    return order

prev_raw_size = 0

def findRoot(addrToOffset):
    for key,val in addrToOffset.items():
        if val == 0:
            return key


def traverse_pt(bin_fp, addrToOffset):
    global leaf_nodes, prev_raw_size
    root_pa = findRoot(addrToOffset)

    traverse_pt_level(4, 0, root_pa, bin_fp, addrToOffset)

    joined_bytes = b''.join(leaf_nodes)
    raw_size = len(joined_bytes)
    prev_raw_size = raw_size
    print("Raw table size:", raw_size)

    compressor = lzma.LZMACompressor()
    compressed_bytes = compressor.compress(joined_bytes)
    compressed_bytes += compressor.flush()
    compressed_size = len(compressed_bytes)
    print("Compressed table size:", compressed_size)
    print("Compression ratio: %fx" % (raw_size / compressed_size))

def counter_stats(dump_dir):
    global PT_node_counter, alloced_page_counter
    print("Counter stats for dump:%s:" % dump_dir)
    print("Page Table nodes:")
    print("\tL4: %d\n\tL3: %d\n\tL2:%d\n\tL1:%d\n" % (PT_node_counter[0],
         PT_node_counter[1], PT_node_counter[2], PT_node_counter[3]))
    print("Allocated pages:")
    print("\t1GB: %d (%dGB)\n\t2MB: %d (%dMB)\n\t4KB: %d(%dKB)" % \
        (alloced_page_counter[0], alloced_page_counter[0],
        alloced_page_counter[1], 2 * alloced_page_counter [1],
        alloced_page_counter[2], 4 * alloced_page_counter [2]))

def reset_counters():
    global PT_node_counter, alloced_page_counter
    PT_node_counter = [0, 0, 0, 0]
    alloced_page_counter = [0, 0, 0]

def multi_base_compression_result(ptrs, delta_width, eff_ptr_size):
    bases = []
    deltas = []
    total_bits = 0
    max_diff = (1 << delta_width) - 1
    for ptr in ptrs:
        if len(bases) == 0:
            bases.append(ptr)
            deltas.append(0)
            continue

        if ptr - bases[-1] <= max_diff:
            deltas.append(ptr - bases[-1])
        else:
            bases.append(ptr)
            deltas.append(0)
    # calculate total bits
    base_select_bits = nearestPow2Order(len(bases))
    total_bits = len(bases) * eff_ptr_size + len(deltas) * (delta_width + base_select_bits)

    return total_bits, bases, base_select_bits, deltas

def num_base_analysis():
    global per_level_ptrs
    sorted_ptrs = [sorted(a) for a in per_level_ptrs]

    # 4KB is 40bit - 12 bit = 28 bit PPN.
    # 2MB is 40bit - 21 bit = 19 bit PPN.
    # How many bits can we use per delta to maximize compression?

    print("Searching optimal 4KB point")
    totalBits = []
    for delta_width in range(8, 25):
        total_bits,bases,baseSelectBits,_ = multi_base_compression_result(sorted_ptrs[2], delta_width, 40-12)
        print ("\tdelta_width: %2db, num_bases: %3d, baseSelect: %db, total_bits: %3db, %3dB" % \
            (delta_width, len(bases), baseSelectBits, total_bits, total_bits/8))
        totalBits.append(total_bits)
    fourK_min= min(totalBits)

    totalBits = []
    print("Searching optimal 2MB point")
    for delta_width in range(5, 17):
        total_bits,bases,baseSelectBits,_ = multi_base_compression_result(sorted_ptrs[1], delta_width, 40-21)
        print ("\tdelta_width: %2db, num_bases: %3d, baseSelect: %db, total_bits: %3db, %3dB" % \
            (delta_width, len(bases), baseSelectBits, total_bits, total_bits/8))
        totalBits.append(total_bits)
    twoM_min= min(totalBits)

    bestCaseBits = fourK_min + twoM_min 
    print("4KB min bytes: %dB, 2MB min bytes: %dB, total %dB" % (fourK_min/8, twoM_min/8, bestCaseBits/8))
    print("Compression ratio: %fx" % (prev_raw_size/(bestCaseBits/8)))


    per_level_ptrs = [ [], [], [] ]


def main():
    dump_dirs = []
    if len(sys.argv) > 1:
        dump_dirs = [a for a in sys.argv[1:]]

    for dump_dir in dump_dirs:
        dump_bin_fp = open(os.path.join(dump_dir, 'pt.bin'), 'rb')
        with open(os.path.join(dump_dir, 'pt.json'), 'r') as fp:
            addrToOffset = json.load(fp) 
        print("Opened files in %s" % dump_dir)

        traverse_pt(dump_bin_fp, addrToOffset)
        counter_stats(dump_dir)
        reset_counters()
        num_base_analysis()


if __name__ == '__main__':
    main()
