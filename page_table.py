
PT_PRESENT  = 1 << 0
PT_RW       = 1 << 1
PT_US       = 1 << 2
PT_PWT      = 1 << 3
PT_PCD      = 1 << 4
PT_A        = 1 << 5
PT_D        = 1 << 6
PT_PS       = 1 << 7
PT_G        = 1 << 8
PT_PAT_UP   = 1 << 12
PT_PAT_DOWN = 1 << 7
PT_XD       = 1 << 63

phys_addrs_bits = 39 # Get from /proc/cpuinfo
UPPER_PA_UNUSED_MASK = ((1 << (64-phys_addrs_bits)) - 1) << phys_addrs_bits
PAGE_OFFSET =  ((1 << 12) - 1)

PAGE_MASK = ~(PAGE_OFFSET | UPPER_PA_UNUSED_MASK)


class Cr3:
    def __init__(self, cr3_int):
        self.pml4_ptr = (cr3_int) & PAGE_MASK

    def get_ptr(self):
        return self.pml4_ptr

#class RawPT_Page: # Raw Page Table binary
#    def __init__(self, raw_binary, va_base):
#        self.va_base = va_base
#        self.raw_page = raw_binary
#        self.links = [None] * 512
#
#class RawPT: # Raw binaries of the page table
#    def __init__(self, pid, cr3):
#        self.pid = pid
#        self.cr3 = cr3
#
#    def register_base(raw_bin):
#        self.root = RawPT_Page(raw_bin, 0)

class PTL: # Page Table Level

    def __init__(self, pt_entry_int, va, level):

        if pt_entry_int & PT_PRESENT:
            self.present = True
        else:
            self.present = False
            pt_entry_int = 0 # Ignore the rest

        if pt_entry_int & PT_RW:
            self.readwrite = True
        else:
            self.readwrite = False

        if pt_entry_int & PT_US:
            self.usersuper = True
        else:
            self.usersuper = False

        if pt_entry_int & PT_PWT:
            self.writethrough = True
        else:
            self.writethrough = False

        if pt_entry_int & PT_PCD:
            self.cachedisabled = True
        else:
            self.cachedisabled = False

        if pt_entry_int & PT_A:
            self.accessed = True
        else:
            self.accessed = False

        if pt_entry_int & PT_XD:
            self.executedisabled = True
        else:
            self.executedisabled = False

        self.next_ptr = pt_entry_int & PAGE_MASK
        self.va = va
        self.level = level

        # Per level specific bits
        if self.level == 3 or self.level == 2:
            if pt_entry_int & PT_PS:
                self.pagesize = True
            else:
                self.pagesize = False

        if self.level < 4:
            if self.level == 1 or self.pagesize:
                if pt_entry_int & PT_D:
                    self.dirty = True
                else:
                    self.dirty = False

                if pt_entry_int & PT_G:
                    self.global_mapping = True
                else:
                    self.global_mapping = False

            if (self.level == 2 or self.level == 3) and self.pagesize:
                if pt_entry_int & PT_PAT_UP:
                    self.pat= True
                    self.next_ptr &= ~PT_PAT_UP
                else:
                    self.pat= False

            elif self.level == 1:
                if pt_entry_int & PT_PAT_DOWN:
                    self.pat= True
                else:
                    self.pat= False

    def isLeaf(self):
        if self.level == 1:
            return True
        elif (self.level == 3 or self.level == 2) and self.pagesize:
            return True
        return False

    def gen_level_str(self):
        if self.level == 4:
            return ''

        string = ', '
        if self.level == 3 or self.level == 2:
            string += 'PS:%d' % self.pagesize

        if self.level < 4:
            if self.level == 1 or self.pagesize:
                string += ', '
                string += 'D:%d, G:%d, PAT:%d' % (self.dirty, self.global_mapping, self.pat)
                string += ', EFF_PTR: 0x%x' % self.get_effective_ptr_bits()

        return string

    def get_ptr(self):
        return self.next_ptr

    def get_effective_ptr_bits(self):
        ptr_bits = self.next_ptr
        ptr_bits >>= (12 + (self.level-1)*9)
        return ptr_bits

    def __str__(self):
        common_str = 'PTL%d: va: 0x%12x, pdp_ptr: 0x%10x, P:%d RW:%d, US:%d, PWT:%d, PCD:%d, A:%d, XD:%d' % \
                (self.level, self.va, self.next_ptr, self.present, self.readwrite, self.usersuper,
                 self.writethrough, self.cachedisabled, self.accessed, self.executedisabled)
        level_specific_str = self.gen_level_str()

        return common_str + level_specific_str
