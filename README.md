# PT_dump

Given the PID, uses the pid2cr3 module to get the physical address CR3 value. Then this python code aims to dump the page table information by using the `/dev/mem` pseudo file offered by the kernel. 