#!/usr/bin/python3

import sys
import os
import json
import page_table as pt

raw_mem_filename = '/dev/mem'
pid2cr3_filename = '/sys/kernel/pid2cr3/pid2cr3'

def pid2cr3(pid):
    with open(pid2cr3_filename, 'w') as fp:
      fp.write("%d" % pid)
    with open(pid2cr3_filename, 'r') as fp:
      cr3 = fp.read()

    return int(cr3, 16)


def get_cr3(pid):
    cr3_int = pid2cr3(pid)
    cr3 = pt.Cr3(cr3_int)
    return cr3


def traverse_pt_level(level, base_va, pt_pa, fp_triplet, addr2Offset_dic):
    if level == 0:
        return

    dumpFp = fp_triplet[0]
    printFp = fp_triplet[1]
    rawFp = fp_triplet[2]

    PTL = pt.PTL

    ret = rawFp.seek(pt_pa, os.SEEK_SET) # 0 for 'from beginning'

    pt_node = rawFp.read(4096)

    if pt_pa in addr2Offset_dic:
        print("pt_pa: 0x%x already exists in dict" % pt_pa)
        sys.exit(1)
    else:
        addr2Offset_dic[pt_pa] = dumpFp.seek(0, os.SEEK_CUR)
        ret = dumpFp.write(pt_node)
        assert(ret == 4096)

    iterate_range = 512
    if level == 4:
        iterate_range = 256 # Don't want to iterate through upper half of VA (kernel space)

    for i in range(iterate_range):
        pte_bin = pt_node[i * 8:i * 8 + 8]
        pte_int = int.from_bytes(pte_bin, byteorder = sys.byteorder)
        va = base_va + i * (1 << (12 + 9*(level-1)))
        pte = PTL(pte_int, va, level)

        if pte.present:
            printFp.write(' ' * (4-level) + "[%3d]\n"%i +  str(pte))
            if pte.isLeaf():
                # Don't attempt to traverse if PT entry is a leaf entry (points to actual page)
                idx = 4-level - 1
            else:
                traverse_pt_level(level-1, va, pte.get_ptr(), fp_triplet, addr2Offset_dic)


def traverse_pt(cr3, fds):
    root_pa = cr3.get_ptr()
    with open(raw_mem_filename, 'rb') as rawFp:
        fd_triplet = (fds[0], fds[2], rawFp)
        rawAddrToOffsetDic = {}
        traverse_pt_level(4, 0, root_pa, fd_triplet, rawAddrToOffsetDic)
        assert(len(rawAddrToOffsetDic) > 0)
        json.dump(rawAddrToOffsetDic, fds[1])

    results_str = "Raw table size: %dB" % fds[0].seek(0, os.SEEK_CUR)
    print(results_str)
    fds[2].write(results_str + '\n')


def chown_dumped_dir(dump_dir):
    uid = int(os.environ['SUDO_UID'])
    gid = int(os.environ['SUDO_GID'])

    os.chown(dump_dir, uid, gid)
    for dirpath, dirnames, filenames in os.walk(dump_dir):  
        for dirname in dirnames:  
            os.chown(os.path.join(dirpath, dirname), uid, gid)
        for filename in filenames:
            os.chown(os.path.join(dirpath, filename), uid, gid)


def main():
    if len(sys.argv) != 3: # [python3 dump_pt.py] [pid] [dump_name]
        print("Please provide the PID to dump and the name of the dump directory")
        print("ex) %s (pid) (dump_name)" % sys.argv[0])
        print("ex) %s 1 init_pt" % sys.argv[0])
        sys.exit(1)

    pid = int(sys.argv[1])
    dump_name = sys.argv[2]

    if os.path.exists(dump_name):
        print("The dump directory (%s) exists! Please try another name" % dump_name)
        sys.exit(1)
    else:
        os.makedirs(dump_name, mode=0o755)

    pt_dump_fd = open(os.path.join(dump_name, 'pt.bin'), mode='xb')
    pt_json_fd = open(os.path.join(dump_name, 'pt.json'), mode='x')
    pt_print_fd = open(os.path.join(dump_name, 'pt.txt'), mode='x')

    fds = [pt_dump_fd, pt_json_fd, pt_print_fd]

    cr3 = get_cr3(pid)
    pid_str = "PID: %d, CR3: %x" % (pid, cr3.get_ptr())
    print(pid_str)
    pt_print_fd.write(pid_str + '\n')

    traverse_pt(cr3, fds)

    for fd in fds:
        fd.close()

    if 'SUDO_UID' in os.environ:
        chown_dumped_dir(dump_name)


if os.geteuid() != 0:
    print("You must run as root")
    sys.exit(1)


if __name__ == '__main__':
    main()
